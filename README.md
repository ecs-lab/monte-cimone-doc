**MCimone usage guide**
===============================

---

**WARNING**: All passwords have been reset (password = username). Please change them with the 'yppasswd' command


---

[A - Compiling applications with slurm](#SBATCH)
---------------------------------------------------------

1.   Now there is installed a new version of **openmpi** compiled with **slurm** support that utilize pmi libraries. Considering that there no more allowed direct SSH access on the nodes, is necessary to use this ne version. Direct SSH access on a node is only allowed if there is a job running on it. When the job ends, the connection is instantly interrupted.
        
            openmpi/4.1.1/gcc-10.3.0-67rz

2.  To compile programs for RISCV64 architecture from a machine with a different architecture, is allowed through the use of the modules already present on the MCimone nodes. \
    Modules already installed on cluster nodes are shared by the administrator and ready to be used (below is a list of already installed modules).
    To use modules from an external machine we have to use slurm by running with **sbatch** an **.sh** script which contain all the required instructions. \
    e.g.: (test.sh file)

        #!/usr/bin/env bash
        #SBATCH --export=NONE
        #SBATCH --job-name=test
        #SBATCH --nodes=2
        #SBATCH --exclusive
        #SBATCH --time=00:20:00
        #SBATCH --output=%x_%A_%a.out
        #SBATCH --error=%x_%A_%a.err

        module use /opt/share/modules/linux-ubuntu21.04-u74mc
        module load openmpi/4.1.1/gcc-10.3.0-67rz

        mpicc -o mpi_hello_world.x mpi_hello_world.c
        

    -   Firstly, before the code of the script, we must declare the slurm configuration using **#SBATCH** tags (configuration described above is only an example)
    -   To use a module you need to indicate absolute path of location in where it is stored, in the script abeve there is an example **module use /opt/share/modules/linux-ubuntu21.04-u74mc**. 
        In this directory there are all the shared modules.
    -   Then you have to define the modules that you want to load among those available.
        In the example above we load only openmpi **module load openmpi/4.1.1/gcc-10.3.0-5hd3**
    -   You now can write the rest of the script
    -   **#SBATCH --export=NONE** line means that you do not want to propagate the environment of your working machine during slurm execution. If you dont't want to propagate the environment, you must have to add this line **. /usr/share/modules/init/bash** immediately after the **#SBATCH** tags. \

    e.g.:

            #!/usr/bin/env bash
            #SBATCH --job-name=test
            #SBATCH --nodes=2
            #SBATCH --exclusive
            #SBATCH --time=00:20:00
            #SBATCH --output=%x_%A_%a.out
            #SBATCH --error=%x_%A_%a.err

            . /usr/share/modules/init/bash

            module use /opt/share/modules/linux-ubuntu21.04-u74mc
            module load openmpi/4.1.1/gcc-10.3.0-67rz

            mpicc -o mpi_hello_world.x mpi_hello_world.c

    - to run the script use this command: **sbatch test.sh**
    - to verify that your job is successfully completed use: **sacct** (list of completed jobs)
    - to verify cluster nodes status use: **sinfo**
            
3. To execute openmpi applications we can proceed in this way:

        #!/usr/bin/env bash
        set -euo pipefail

        #SBATCH --nodes=8
        #SBATCH --tasks-per-node=4
        #SBATCH --cpus-per-task=1
        #SBATCH --export=NONE
        #SBATCH --output=%x_%A_%a.out
        #SBATCH --error=%x_%A_%a.err

        source /usr/share/modules/init/bash

        module use /opt/share/modules/linux-ubuntu21.04-u74mc
        module load openmpi/4.1.1/gcc-10.3.0-67rz

        srun -N 8 -n 32 mpi_hello_world.x


4.  In you don't need specific configuration that has to be write in the script, you can use directly the **srun** command
            
        srun -N 8 -n 32 mpi_hello_world.x

-   Is now mandatory use **srun** to execute **openmpi** applications. Since direct use of SSH is not allowed, execution via **mpirun** would fail.

5.  Currently there are already different shared modules ready to be used by any user. Below is the list:

        ---------------- /opt/share/modules/linux-ubuntu21.04-u74mc ----------------
        boost/1.77.0/gcc-10.3.0-lnfd          ncurses/6.2/gcc-10.3.0-76en
        cmake/3.21.4/gcc-10.3.0-7n6u          netlib-lapack/3.9.1/gcc-10.3.0-adbj
        curl/7.80.0/gcc-10.3.0-kmng           netlib-scalapack/2.1.0/gcc-10.3.0-dtgk
        fftw/3.3.10/gcc-10.3.0-7p2j           ninja/1.10.2/gcc-10.3.0-sg37
        gcc/10.3.0/gcc-10.3.0-gyqt            numactl/2.0.14/gcc-10.3.0-t7i4
        gnuconfig/2021-08-14/gcc-10.3.0-qinj  openblas/0.3.18/gcc-10.3.0-43ek
        hpl/2.3/gcc-10.3.0-2yqr               openmpi/4.1.1/gcc-10.3.0-5hd3
        hwloc/2.6.0/gcc-10.3.0-ahm7           openmpi/4.1.1/gcc-10.3.0-67rz
        libevent/2.1.12/gcc-10.3.0-wdtj       pcre/8.44/gcc-10.3.0-45r4
        libffi/3.3/gcc-10.3.0-tvmd            perl/5.32.1/gcc-10.3.0-37gt
        libgcrypt/1.9.3/gcc-10.3.0-bvrr       pkg-config/0.29.2/gcc-10.3.0-2jlo
        libgpg-error/1.42/gcc-10.3.0-tmfa     py-setuptools/58.2.0/gcc-10.3.0-v4s7
        libiconv/1.16/gcc-10.3.0-yxhg         readline/8.1/gcc-10.3.0-ljug
        libpciaccess/0.16/gcc-10.3.0-vnol     slurm/20-11-4-1/gcc-10.3.0-zbud
        libxml2/2.9.12/gcc-10.3.0-y2z6        stream/5.10/gcc-10.3.0-aaml
        lz4/1.9.3/gcc-10.3.0-f3yl             util-macros/1.19.3/gcc-10.3.0-m6is
        meson/0.60.0/gcc-10.3.0-ttz6          zlib/1.2.11/gcc-10.3.0-pzrj

    You can obtain the list of available module by using this command: 

        module avail

6.  If you need a specific package, that is not already installed on the system, contact the administrator that will make it available.

7. If you need to work directly on a MCimone node, you can access an interactive shell with this command: **srun -n 1 -N 1 -p cluster -w mcimone-node-(nodeNumber) --pty /bin/bash -l**.
   
[B - Change password](#PASS)
---------------------------------------------------------

1. Is reccomended to use **yppasswd** to change own password. Using this command the password modification will be automatically applied on all cluster nodes. **passwd** command stil works, but the synchronization on the cluster will not be immediate.
